const gulp = require('gulp'),
	uglify = require('gulp-uglify-es').default

gulp.task('dist', () => {
	gulp.src(['./lib/crossroads-fetch-accounts.js'])
		.pipe(uglify())
		.pipe(gulp.dest('dist'))
})

gulp.task('lint', () => {
	const eslint = require('gulp-eslint')
	return gulp.src(['**/*.js','!node_modules/**'])
		.pipe(eslint({ignorePattern: 'dist'}))
		.pipe(eslint.format())
})

gulp.task('publish', ['lint', 'dist'])